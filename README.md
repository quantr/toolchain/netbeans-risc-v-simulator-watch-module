Click one button to add all registers to watch window. To install, go to https://netbeans.quantr.hk

![](https://peter.quantr.hk/wp-content/uploads/2024/01/netbeans-riscv-simulator-watch-module.png)

# Developer

Peter <peter@quantr.hk>

