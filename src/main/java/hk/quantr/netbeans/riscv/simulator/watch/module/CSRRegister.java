/*
 * Copyright 2020 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.riscv.simulator.watch.module;

import java.math.BigInteger;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class CSRRegister implements Register {

	private int name;
	public BigInteger value;
//	public CSRRegister linkedRegister;

	public CSRRegister(int name, long value) {
		this.name = name;
		this.value = BigInteger.valueOf(value);
	}

	public CSRRegister(int name, long value, CSRRegister linkedRegister) {
		this.name = name;
		this.value = BigInteger.valueOf(value);
//		this.linkedRegister = linkedRegister;
	}

	@Override
	public void setValue(long value) {
		this.value = BigInteger.valueOf(value).and(new BigInteger("ffffffffffffffff", 16));

//		if (name.equals("100") || name.equals("300")) {
//			long sie = CommonLib.getBit(value, 1);
//			if (sie == 0) {
//				linkedRegister.value = linkedRegister.value.clearBit(1);
//			} else {
//				linkedRegister.value = linkedRegister.value.setBit(1);
//			}
//			long spie = CommonLib.getBit(value, 5);
//			if (spie == 0) {
//				linkedRegister.value = linkedRegister.value.clearBit(5);
//			} else {
//				linkedRegister.value = linkedRegister.value.setBit(5);
//			}
//		}
//		if (CPUState.priv == CPUState.MACHINE_MODE) {
//			if (name.equals("104")) {
//				linkedRegister.setValue(linkedRegister.value.or(BigInteger.valueOf(value)));
//			}
//		}
	}

	@Override
	public void setValue(BigInteger value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return String.format("%d = %016x", name, getValue());
	}

	@Override
	public String getHexString() {
		return getValue().toString(16);
	}

	@Override
	public BigInteger getValue() {
//		if (name.equals("300")) {
//			if (CPUState.priv == CPUState.MACHINE_MODE) {
//				return value.and(BigInteger.valueOf(0xffffffffffffffdfl));
//			} else if (CPUState.priv == CPUState.SUPERVISOR_MODE) {
//				if (value.and(BigInteger.valueOf(0xffffffffffffff7fl)).longValue()==0xa000009a0l || value.longValue()==0xa000009a0l){
//					System.out.println("fuck");
//				}
//				return value.and(BigInteger.valueOf(0xffffffffffffff7fl));
//			}else{
//				System.out.println("FUCK");
//				return value;
//			}
//		} else {
		return value;
//		}
	}

	@Override
	public String getName() {
		return String.valueOf(name);
	}

}
