package hk.quantr.netbeans.riscv.simulator.watch.module;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedHashMap;
import org.netbeans.api.debugger.DebuggerManager;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;

@ActionID(
		category = "Navigate",
		id = "hk.quantr.netbeans.riscv.simulator.watch.module.AddRiscvWatchAction"
)
@ActionRegistration(
		displayName = "#CTL_AddRiscvWatchAction"
)
@ActionReference(path = "Menu/Navigate", position = 0)
@Messages("CTL_AddRiscvWatchAction=Add RISC-V Watch")
public final class AddRiscvWatchAction implements ActionListener {

	public LinkedHashMap<String, Register> registers;

	@Override
	public void actionPerformed(ActionEvent e) {
		if (registers == null) {
			try {
				registers = new LinkedHashMap<>();

				registers.put("pc", new GeneralRegister("pc", 0l));

				//General Purpose Registers
				for (int x = 0; x <= 31; x++) {
					registers.put("x" + x, new GeneralRegister("x" + x, 0x0));
				}
				registers.put("zero", registers.get("x0"));
				registers.put("ra", registers.get("x1"));
				registers.put("sp", registers.get("x2"));
				registers.put("gp", registers.get("x3"));
				registers.put("tp", registers.get("x4"));
				registers.put("t0", registers.get("x5"));
				registers.put("t1", registers.get("x6"));
				registers.put("t2", registers.get("x7"));
				registers.put("s0", registers.get("x8"));
				registers.put("s1", registers.get("x9"));
				registers.put("a0", registers.get("x10"));
				registers.put("a1", registers.get("x11"));
				registers.put("a2", registers.get("x12"));
				registers.put("a3", registers.get("x13"));
				registers.put("a4", registers.get("x14"));
				registers.put("a5", registers.get("x15"));
				registers.put("a6", registers.get("x16"));
				registers.put("a7", registers.get("x17"));
				registers.put("s2", registers.get("x18"));
				registers.put("s3", registers.get("x19"));
				registers.put("s4", registers.get("x20"));
				registers.put("s5", registers.get("x21"));
				registers.put("s6", registers.get("x22"));
				registers.put("s7", registers.get("x23"));
				registers.put("s8", registers.get("x24"));
				registers.put("s9", registers.get("x25"));
				registers.put("s10", registers.get("x26"));
				registers.put("s11", registers.get("x27"));
				registers.put("t3", registers.get("x28"));
				registers.put("t4", registers.get("x29"));
				registers.put("t5", registers.get("x30"));
				registers.put("t6", registers.get("x31"));

				//Floating Point Registers
				for (int x = 0; x <= 31; x++) {
					registers.put("f" + x, new GeneralRegister("f" + x, 0x0));
				}
				for (int x = 0; x <= 7; x++) {
					registers.put("ft" + x, registers.get("f" + x));
				}
				for (int x = 0; x <= 1; x++) {
					registers.put("fs" + x, registers.get("f" + (x + 8)));
				}
				for (int x = 0; x <= 7; x++) {
					registers.put("fa" + x, registers.get("f" + (x + 10)));
				}
				for (int x = 2; x <= 11; x++) {
					registers.put("fs" + x, registers.get("f" + (x + 16)));
				}
				for (int x = 8; x <= 11; x++) {
					registers.put("ft" + x, registers.get("f" + (x + 20)));
				}

				//CSR Registers. doc from https://five-embeddev.com/riscv-isa-manual/latest/priv-csrs.html#csrrwpriv
				registers.put("ustatus", new CSRRegister(0x000, 0x0));
				registers.put("uie", new CSRRegister(0x004, 0x0));
				registers.put("utvec", new CSRRegister(0x005, 0x0));
				registers.put("uscratch", new CSRRegister(0x040, 0x0));
				registers.put("uepc", new CSRRegister(0x041, 0x0));
				registers.put("ucasue", new CSRRegister(0x042, 0x0));
				registers.put("utval", new CSRRegister(0x043, 0x0));
				registers.put("uip", new CSRRegister(0x044, 0x0));
				registers.put("fflags", new CSRRegister(0x001, 0x0));
				registers.put("frm", new CSRRegister(0x002, 0x0));
				registers.put("fcsr", new CSRRegister(0x003, 0x0));
				registers.put("cycle", new CSRRegister(0xC00, 0x0));
				registers.put("time", new CSRRegister(0xC01, 0x0));
				registers.put("instret", new CSRRegister(0xC02, 0x0));
				for (int x = 3; x <= 31; x++) {
					registers.put("hpmcounter" + x, new CSRRegister(0xC0 + x, 0x0));
				}
				registers.put("cycleh", new CSRRegister(0xC80, 0x0));
				registers.put("timeh", new CSRRegister(0xC81, 0x0));
				registers.put("instreth", new CSRRegister(0xC82, 0x0));
				for (int x = 3; x <= 31; x++) {
					registers.put("hpmcounter" + x + "h", new CSRRegister(0xC8 + x, 0x0));
				}
//		registers.put("sstatus", new CSRRegister(0x100, 0x0));
				registers.put("sedeleg", new CSRRegister(0x102, 0x0));
				registers.put("sideleg", new CSRRegister(0x103, 0x0));
//		registers.put("sie", new CSRRegister(0x104, 0x0));
				registers.put("stvec", new CSRRegister(0x105, 0x0));
				registers.put("scounteren", new CSRRegister(0x106, 0x0));
				registers.put("sscratch", new CSRRegister(0x140, 0x0));
				registers.put("sepc", new CSRRegister(0x141, 0x0));
				registers.put("scause", new CSRRegister(0x142, 0x0));
				registers.put("stval", new CSRRegister(0x143, 0x0));
				registers.put("sip", new CSRRegister(0x144, 0x0));
				registers.put("satp", new CSRRegister(0x180, 0x0));
				registers.put("scontext", new CSRRegister(0x5A8, 0x0));
				registers.put("hstatus", new CSRRegister(0x600, 0x0));
				registers.put("hedeleg", new CSRRegister(0x602, 0x0));
				registers.put("hideleg", new CSRRegister(0x603, 0x0));
				registers.put("hie", new CSRRegister(0x604, 0x0));
				registers.put("hcounteren", new CSRRegister(0x606, 0x0));
				registers.put("hgeie", new CSRRegister(0x607, 0x0));
				registers.put("htval", new CSRRegister(0x643, 0x0));
				registers.put("hip", new CSRRegister(0x644, 0x0));
				registers.put("hvip", new CSRRegister(0x645, 0x0));
				registers.put("htinst", new CSRRegister(0x64A, 0x0));
				registers.put("hgeip", new CSRRegister(0xE12, 0x0));
				registers.put("hgatp", new CSRRegister(0x680, 0x0));
				registers.put("hcontext", new CSRRegister(0x6A8, 0x0));
				registers.put("htimedelta", new CSRRegister(0x605, 0x0));
				registers.put("htimedeltah", new CSRRegister(0x615, 0x0));
				registers.put("vsstatus", new CSRRegister(0x200, 0x0));
				registers.put("vsie", new CSRRegister(0x204, 0x0));
				registers.put("vstvec", new CSRRegister(0x205, 0x0));
				registers.put("vsscratch", new CSRRegister(0x240, 0x0));
				registers.put("vsepc", new CSRRegister(0x241, 0x0));
				registers.put("vscause", new CSRRegister(0x242, 0x0));
				registers.put("vstval", new CSRRegister(0x243, 0x0));
				registers.put("vsip", new CSRRegister(0x244, 0x0));
				registers.put("vsatp", new CSRRegister(0x280, 0x0));
				registers.put("mvendorid", new CSRRegister(0xF11, 0x0)); //fixed id for hart 
				registers.put("marchid", new CSRRegister(0xF12, 0x0)); // fixed id for hart
				registers.put("mimpid", new CSRRegister(0xF13, 0x0));// fixed value 
				registers.put("mhartid", new CSRRegister(0xF14, 0x0));
				registers.put("mstatus", new CSRRegister(0x300, 0x0)); // machine mode
				registers.put("misa", new CSRRegister(0x301, 0x0));
				registers.put("medeleg", new CSRRegister(0x302, 0x0));
				registers.put("mideleg", new CSRRegister(0x303, 0x0));
				registers.put("mie", new CSRRegister(0x304, 0x0));
				registers.put("mtvec", new CSRRegister(0x305, 0x0));
				registers.put("mcounteren", new CSRRegister(0x306, 0x0));
				registers.put("mstatush", new CSRRegister(0x310, 0x0));
				registers.put("mscratch", new CSRRegister(0x340, 0x0));
				registers.put("mepc", new CSRRegister(0x341, 0x0));
				registers.put("mcause", new CSRRegister(0x342, 0x0));
				registers.put("mtval", new CSRRegister(0x343, 0x0));
				registers.put("mip", new CSRRegister(0x344, 0x0));
				registers.put("mtinst", new CSRRegister(0x34A, 0x0));
				registers.put("mtval2", new CSRRegister(0x34B, 0x0));
				for (int x = 0; x <= 3; x++) {
					registers.put("pmpcfg" + x, new CSRRegister(0x3A + x, 0x0));
				}
				for (int x = 0; x <= 15; x++) {
					registers.put("pmpaddr" + x, new CSRRegister(0x3B0 + x, 0x0));
				}
				registers.put("mcycle", new CSRRegister(0xB00, 0x0));
				registers.put("minstret", new CSRRegister(0xB02, 0x0));
				for (int x = 3; x <= 31; x++) {
					registers.put("mhpmcounter" + x, new CSRRegister(0xB00 + x, 0x0));
				}
				registers.put("mcycleh", new CSRRegister(0xB80, 0x0));
				registers.put("minstreth", new CSRRegister(0xB82, 0x0));
				for (int x = 3; x <= 31; x++) {
					registers.put("mhpmcounter" + x + "h", new CSRRegister(0xB80 + x, 0x0));
				}
				registers.put("mcountinhibit", new CSRRegister(0x320, 0x0));
				for (int x = 3; x <= 31; x++) {
					registers.put("mhpmevent" + x, new CSRRegister(0x320 + x, 0x0));
				}
				registers.put("tselect", new CSRRegister(0x7A0, 0x0));
				registers.put("tdata1", new CSRRegister(0x7A1, 0x0));
				registers.put("tdata2", new CSRRegister(0x7A2, 0x0));
				registers.put("tdata3", new CSRRegister(0x7A3, 0x0));
				registers.put("mcontext", new CSRRegister(0x7A8, 0x0));
				registers.put("dcsr", new CSRRegister(0x7B0, 0x0));
				registers.put("dpc", new CSRRegister(0x7B1, 0x0));
				registers.put("dscratch0", new CSRRegister(0x7B2, 0x0));
				registers.put("dscratch1", new CSRRegister(0x7B3, 0x0));

				//Vector registers
				for (int x = 0; x <= 31; x++) {
					registers.put("v" + x, new GeneralRegister("read_and_write", 0x0));
				}

				//VCSR
				registers.put("vtype", new GeneralRegister("read_and_write", 0x0));
				registers.put("vsew", new GeneralRegister("read_and_write", 0x0));
				registers.put("vlmul", new GeneralRegister("read_and_write", 0x0));
				registers.put("vta", new GeneralRegister("read_and_write", 0x0));
				registers.put("vma", new GeneralRegister("read_and_write", 0x0));
				registers.put("vl", new GeneralRegister("read_and_write", 0x0));
				registers.put("vlenb", new GeneralRegister("read_and_write", 0x0));
				registers.put("vstart", new GeneralRegister("read_and_write", 0x0));
				registers.put("vxrm", new GeneralRegister("read_and_write", 0x0));
				registers.put("vxsat", new GeneralRegister("read_and_write", 0x0));
				registers.put("vcsr", new GeneralRegister("read_and_write", 0x0));
			} catch (Exception ex) {
				Exceptions.printStackTrace(ex);
			}
		}
		for (String name : registers.keySet()) {
			DebuggerManager.getDebuggerManager().createWatch("registers.get(\"" + name + "\").getHexString()");
		}
	}
}
